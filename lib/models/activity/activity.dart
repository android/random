import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/config/application_config.dart';
import 'package:random/models/activity/game_board.dart';
import 'package:random/models/activity/game_cell.dart';

class Activity {
  GameBoard board;
  ActivitySettings activitySettings;
  bool isRunning = false;
  bool isFinished = false;
  int availableBlocksCount = 0;
  int movesCount = 0;
  int score = 0;

  Activity({
    required this.board,
    required this.activitySettings,
    this.isRunning = false,
  });

  factory Activity.createNull() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
      // Base data
      board: GameBoard.createNull(),
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      board: GameBoard.createRandom(newActivitySettings),
      activitySettings: newActivitySettings,
      isRunning: true,
    );
  }

  void stop() {
    isRunning = false;
    isFinished = true;
  }

  bool get canBeResumed => !isFinished && isRunning;

  GameCell getCell(int x, int y) {
    return board.cells[y][x];
  }

  int? getCellValue(int x, int y) {
    return getCell(x, y).value;
  }

  void updateCellValue(int x, int y, int? value) {
    board.cells[y][x].value = value;
  }

  void setRandomCellValue(int x, int y, ActivitySettings settings) {
    final int maxValue = settings.getAsInt(ApplicationConfig.parameterCodeColorsCount);
    final rand = Random();
    int value = 1 + rand.nextInt(maxValue);

    board.cells[y][x].value = value;
  }

  void increaseMovesCount() {
    movesCount += 1;
  }

  void increaseScore(int? count) {
    score += (count ?? 0);
  }

  @override
  String toString() {
    return 'Game(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'board': board.toJson(),
      'settings': activitySettings.toJson(),
      'isRunning': isRunning,
      'isFinished': isFinished,
      'availableBlocksCount': availableBlocksCount,
      'movesCount': movesCount,
      'score': score,
    };
  }

  void dump() {
    GameBoard.printGrid(board.cells);
    printlog(activitySettings.toString());
    printlog(toString());
  }
}

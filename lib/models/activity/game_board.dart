import 'dart:math';

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/config/application_config.dart';
import 'package:random/models/activity/game_cell.dart';

class GameBoard {
  final List<List<GameCell>> cells;

  GameBoard({
    required this.cells,
  });

  factory GameBoard.createNull() {
    return GameBoard(cells: []);
  }

  factory GameBoard.createRandom(ActivitySettings activitySettings) {
    final int boardSizeHorizontal =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int boardSizeVertical =
        activitySettings.getAsInt(ApplicationConfig.parameterCodeBoardSize);
    final int maxValue = activitySettings.getAsInt(ApplicationConfig.parameterCodeColorsCount);

    final rand = Random();

    List<List<GameCell>> grid = [];
    for (var rowIndex = 0; rowIndex < boardSizeVertical; rowIndex++) {
      List<GameCell> row = [];
      for (var colIndex = 0; colIndex < boardSizeHorizontal; colIndex++) {
        int value = 1 + rand.nextInt(maxValue);
        row.add(GameCell(value));
      }
      grid.add(row);
    }
    printGrid(grid);

    return GameBoard(cells: grid);
  }

  static printGrid(List<List<GameCell>> cells) {
    printlog('');
    printlog('-------');
    for (var rowIndex = 0; rowIndex < cells.length; rowIndex++) {
      String row = '';
      for (var colIndex = 0; colIndex < cells[rowIndex].length; colIndex++) {
        row += cells[rowIndex][colIndex].value.toString();
      }
      printlog(row);
    }
    printlog('-------');
    printlog('');
  }

  @override
  String toString() {
    return 'Board(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'cells': cells.toString(),
    };
  }
}

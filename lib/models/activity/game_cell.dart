class GameCell {
  int? value;

  GameCell(
    this.value,
  );

  @override
  String toString() {
    return 'Cell(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'value': value,
    };
  }
}

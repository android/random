import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

enum InterfaceType {
  basic,
  expert,
}

class InterfaceTypes {
  static List<Widget> selectWidgets = <Widget>[
    const Text('interface_type_basic').tr(),
    const Text('interface_type_expert').tr(),
  ];
}

import 'dart:convert';

class ApiData {
  final int? number;
  final String? md5;
  final DateTime? updatedAt;

  const ApiData({
    required this.number,
    required this.md5,
    required this.updatedAt,
  });

  factory ApiData.fromJson(Map<String, dynamic>? json) {
    return ApiData(
      number: (json?['number'] != null) ? (json?['number'] as int) : null,
      md5: (json?['md5'] != null) ? (json?['md5'] as String) : null,
      updatedAt: (json?['now'] != null) ? DateTime.parse(json?['now']) : null,
    );
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      'number': number ?? 0,
      'md5': md5 ?? '',
      'updatedAt': updatedAt?.toString(),
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

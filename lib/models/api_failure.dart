class ApiFailure {
  final String message;
  final String code;

  const ApiFailure({
    this.message = '',
    this.code = '',
  });
}

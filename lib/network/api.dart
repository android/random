import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/models/api_failure.dart';

class ApiService {
  final Dio _dio = Dio();

  final String baseUrl = 'https://tools.harrault.fr/tools/api';

  Future<Response?> getData() async {
    String url = '$baseUrl/get.php';
    try {
      printlog('fetching api data... $url');
      final Response response = await _dio.get(url);
      printlog('ok got api response.');
      printlog(response.toString());
      return response;
    } on SocketException {
      throw const ApiFailure(message: 'Failed to reach API endpoint.');
    } catch (err) {
      printlog("Error (getData): $err");
      throw ApiFailure(message: "$err");
    }
  }
}

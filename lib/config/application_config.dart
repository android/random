import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:random/ui/pages/api.dart';
import 'package:random/ui/pages/camera.dart';
import 'package:random/ui/pages/demo.dart';
import 'package:random/ui/pages/game.dart';
import 'package:random/ui/pages/graph.dart';

import 'package:random/cubit/activity/activity_cubit.dart';
import 'package:random/ui/parameters/parameter_painter_board_size.dart';
import 'package:random/ui/parameters/parameter_painter_colors_count.dart';

class ApplicationConfig {
  // activity parameter: board size
  static const String parameterCodeBoardSize = 'boardSize';
  static const String boardSizeValueSmall = '6';
  static const String boardSizeValueMedium = '10';
  static const String boardSizeValueLarge = '16';
  static const String boardSizeValueExtraLarge = '24';

  // activity parameter: colors count
  static const String parameterCodeColorsCount = 'colorsCount';
  static const String colorsCountVeryLow = '4';
  static const String colorsCountLow = '5';
  static const String colorsCountMedium = '6';
  static const String colorsCountHigh = '7';

  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexDemo = 1;
  static const int activityPageIndexGame = 2;
  static const int activityPageIndexCamera = 3;
  static const int activityPageIndexGraph = 4;
  static const int activityPageIndexApi = 5;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Random application',
    activitySettings: [
      // board size
      ApplicationSettingsParameter(
        code: parameterCodeBoardSize,
        values: [
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueSmall,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueMedium,
            color: Colors.orange,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueLarge,
            color: Colors.red,
          ),
          ApplicationSettingsParameterItemValue(
            value: boardSizeValueExtraLarge,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) => ParameterPainterBoardSize(
          context: context,
          value: value,
        ),
      ),

      // colors count
      ApplicationSettingsParameter(
        code: parameterCodeColorsCount,
        values: [
          ApplicationSettingsParameterItemValue(
            value: colorsCountVeryLow,
            color: Colors.green,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountLow,
            color: Colors.orange,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountMedium,
            color: Colors.red,
            isDefault: true,
          ),
          ApplicationSettingsParameterItemValue(
            value: colorsCountHigh,
            color: Colors.purple,
          ),
        ],
        customPainter: (context, value) => ParameterPainterColorsCount(
          context: context,
          value: value,
        ),
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      displayBottomNavBar: true,
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexDemo: ActivityPageItem(
          code: 'page_demo',
          icon: Icon(UniconsLine.eye),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return PageDemo();
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
        activityPageIndexCamera: ActivityPageItem(
          code: 'page_camera',
          icon: Icon(UniconsLine.camera),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return PageCamera();
          },
        ),
        activityPageIndexGraph: ActivityPageItem(
          code: 'page_graph',
          icon: Icon(UniconsLine.pen),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return PageGraph();
          },
        ),
        activityPageIndexApi: ActivityPageItem(
          code: 'page_api',
          icon: Icon(UniconsLine.link),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return PageApi();
          },
        ),
      },
    ),
  );
}

part of 'settings_cubit.dart';

@immutable
class SettingsState extends Equatable {
  const SettingsState({
    this.apiUrl,
    this.securityToken,
    this.interfaceType,
  });

  final String? apiUrl;
  final String? securityToken;
  final InterfaceType? interfaceType;

  @override
  List<String?> get props => <String?>[
        apiUrl,
        securityToken,
        interfaceType.toString(),
      ];

  Map<String, String?> get values => <String, String?>{
        'apiUrl': apiUrl,
        'securityToken': securityToken,
        'interfaceType': interfaceType.toString(),
      };
}

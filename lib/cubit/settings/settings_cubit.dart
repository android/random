import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:flutter/material.dart';

import 'package:random/models/interface_type.dart';

part 'settings_state.dart';

class SettingsCubit extends HydratedCubit<SettingsState> {
  SettingsCubit() : super(const SettingsState());

  Object getSetting(String key, [String? defaultValue]) {
    if (state.values.keys.contains(key)) {
      return state.values[key] ?? defaultValue ?? '';
    }

    return defaultValue ?? '';
  }

  String getApiUrl() {
    return state.apiUrl ?? '';
  }

  String getSecurityToken() {
    return state.securityToken ?? '';
  }

  InterfaceType getInterfaceType() {
    return state.interfaceType ?? InterfaceType.basic;
  }

  void setValues({
    String? apiUrl,
    String? securityToken,
    InterfaceType? interfaceType,
  }) {
    emit(SettingsState(
      apiUrl: apiUrl ?? state.apiUrl,
      securityToken: securityToken ?? state.securityToken,
      interfaceType: interfaceType ?? state.interfaceType,
    ));
  }

  @override
  SettingsState? fromJson(Map<String, dynamic> json) {
    String apiUrl = json['apiUrl'] as String;
    String securityToken = json['securityToken'] as String;
    InterfaceType interfaceType;

    switch (json['interfaceType'] as String) {
      case 'InterfaceType.basic':
        interfaceType = InterfaceType.basic;
        break;
      case 'InterfaceType.expert':
        interfaceType = InterfaceType.expert;
        break;
      default:
        interfaceType = InterfaceType.basic;
    }

    return SettingsState(
      apiUrl: apiUrl,
      securityToken: securityToken,
      interfaceType: interfaceType,
    );
  }

  @override
  Map<String, String>? toJson(SettingsState state) {
    return <String, String>{
      'apiUrl': state.apiUrl ?? '',
      'securityToken': state.securityToken ?? '',
      'interfaceType': state.interfaceType.toString(),
    };
  }
}

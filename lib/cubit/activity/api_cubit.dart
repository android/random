import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/models/api_failure.dart';
import 'package:random/models/api_data.dart';
import 'package:random/repository/api.dart';

part 'api_state.dart';

class ApiDataCubit extends HydratedCubit<ApiDataState> {
  final ApiRepository apiRepository;

  ApiDataCubit({required this.apiRepository}) : super(ApiDataFetchInitial());

  void customEmit(ApiDataState state) {
    String stateAsString = '';
    if (state is ApiDataFetchLoaded) {
      stateAsString = 'ApiDataFetchLoaded';
    } else {
      if (state is ApiDataFetchLoading) {
        stateAsString = 'ApiDataFetchLoading';
      } else {
        if (state is ApiDataFetchError) {
          stateAsString = 'ApiDataFetchError';
        } else {
          stateAsString = 'unknown';
        }
      }
    }
    printlog('emit new state: $stateAsString');

    emit(state);
  }

  Future<ApiData> fetchApiData() async {
    customEmit(ApiDataFetchLoading());
    try {
      final ApiData apiData = await apiRepository.getApiData();
      customEmit(ApiDataFetchLoaded(data: apiData));
      return apiData;
    } on ApiFailure catch (err) {
      customEmit(ApiDataFetchError(failure: err));
    } catch (err) {
      printlog("Error (fetchApiData): $err");
    }
    return ApiData.fromJson({});
  }

  @override
  ApiDataState? fromJson(Map<String, dynamic> json) {
    return ApiDataState(
      data: ApiData.fromJson(json['api-data']),
    );
  }

  @override
  Map<String, Object?>? toJson(ApiDataState state) {
    return <String, Object?>{
      'api-data': state.data?.toJson(),
    };
  }
}

part of 'data_cubit.dart';

@immutable
class DataState extends Equatable {
  const DataState({
    this.counter,
  });

  final int? counter;

  @override
  List<Object?> get props => <Object?>[
        counter,
      ];
}

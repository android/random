import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:flutter/material.dart';

part 'data_state.dart';

class DataCubit extends HydratedCubit<DataState> {
  DataCubit() : super(const DataState());

  void getData(DataState state) {
    emit(state);
  }

  void updateCounter(int delta) {
    emit(DataState(counter: (state.counter ?? 0) + delta));
  }

  @override
  DataState? fromJson(Map<String, dynamic> json) {
    int counter = json['counter'] as int;

    return DataState(
      counter: counter,
    );
  }

  @override
  Map<String, int>? toJson(DataState state) {
    return <String, int>{
      'counter': state.counter ?? 0,
    };
  }
}

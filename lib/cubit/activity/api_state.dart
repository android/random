part of 'api_cubit.dart';

class ApiDataState extends Equatable {
  final ApiData? data;

  const ApiDataState({
    this.data,
  });

  @override
  List<Object> get props => [];
}

class ApiDataFetchInitial extends ApiDataState {}

class ApiDataFetchLoading extends ApiDataState {}

class ApiDataFetchLoaded extends ApiDataState {
  const ApiDataFetchLoaded({
    required super.data,
  });

  @override
  List<Object> get props => [data!];
}

class ApiDataFetchError extends ApiDataState {
  final ApiFailure failure;

  const ApiDataFetchError({
    required this.failure,
  });

  @override
  List<Object> get props => [failure];
}

import 'dart:io';

import 'package:path/path.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

class PictureStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<String> _localFilePath(String name) async {
    final path = await _localPath;

    return '$path/$name';
  }

  Future<File> moveFile(File sourceFile, String newPath) async {
    try {
      return await sourceFile.rename(newPath);
    } on FileSystemException catch (e) {
      printlog('Found exception while moving file: $e');
      final newFile = await sourceFile.copy(newPath);
      await sourceFile.delete();
      return newFile;
    }
  }

  Future<File> writeCounter(File sourceFile) async {
    final targetFile = await _localFilePath(basename(sourceFile.path));

    return moveFile(sourceFile, targetFile);
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/cubit/activity/data_cubit.dart';
import 'package:random/cubit/settings/settings_cubit.dart';
import 'package:random/ui/widgets/custom_title.dart';

class PageDemo extends StatelessWidget {
  const PageDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      physics: const BouncingScrollPhysics(),
      children: <Widget>[
        const SizedBox(height: 8),
        const CustomTitle(text: 'TOP'),
        const SizedBox(height: 20),
        StyledContainer(
          child: persistedCounterBlock(BlocProvider.of<DataCubit>(context)),
        ),
        const SizedBox(height: 8),
        StyledContainer(
          borderRadius: 0,
          borderWidth: 12,
          // depth: 8,
          lowerColor: Colors.red,
          upperColor: Colors.yellow,
          child: testBlocConsumer(),
        ),
        const SizedBox(height: 8),
        StyledContainer(
          borderRadius: 10,
          borderWidth: 30,
          depth: 20,
          lowerColor: Colors.blueGrey,
          upperColor: Colors.blue,
          child: testBlocBuilder(),
        ),
        const SizedBox(height: 8),
        fakeApiCall(),
        const SizedBox(height: 8),
        const CustomTitle(text: 'BOTTOM'),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            StyledButton.text(
              caption: 'ABC',
              color: Colors.yellow,
              onPressed: () {
                printlog('A');
              },
            ),
            StyledButton.text(
              caption: '❤️‍🔥',
              color: Colors.red,
              onPressed: () {
                printlog('fire!');
              },
            ),
            StyledButton.text(
              caption: '⭐',
              color: Colors.green,
              onPressed: () {
                printlog('star!');
              },
            ),
            StyledButton.text(
              caption: '🧁',
              color: Colors.blue,
              onPressed: () {
                printlog('Cupcake');
              },
            ),
            StyledButton.icon(
              icon: Icon(UniconsLine.setting),
              color: Colors.purple,
              iconSize: 20,
              onPressed: () {
                printlog('icon');
              },
            ),
          ],
        ),
        const SizedBox(height: 8),
        StyledButton.text(
          caption: 'BUTTON - LARGE',
          color: Colors.orange,
          onPressed: () {
            printlog('large button');
          },
        ),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
              child: Text('ABC'),
              onPressed: () {
                printlog('TextButton');
              },
            ),
            OutlinedButton(
              child: Text('❤️‍🔥'),
              onPressed: () {
                printlog('OutlinedButton');
              },
            ),
            FilledButton(
              child: Text('⭐'),
              onPressed: () {
                printlog('FilledButton');
              },
            ),
            ElevatedButton(
              child: Text('🧁'),
              onPressed: () {
                printlog('ElevatedButton');
              },
            ),
          ],
        ),
      ],
    );
  }

  Widget persistedCounterBlock(DataCubit dataCubit) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        IconButton(
          icon: const Icon(UniconsSolid.arrow_circle_down),
          color: appTheme.primaryColor,
          onPressed: () => dataCubit.updateCounter(-1),
        ),
        testBlocConsumer(),
        IconButton(
          icon: const Icon(UniconsSolid.arrow_circle_up),
          color: appTheme.primaryColor,
          onPressed: () => dataCubit.updateCounter(1),
        ),
      ],
    );
  }

  Widget fakeApiCall() {
    return BlocBuilder<SettingsCubit, SettingsState>(
      builder: (context, settingsSate) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('apiUrl: ${settingsSate.apiUrl}'),
            Text('securityToken: ${settingsSate.securityToken}'),
            Text('interfaceType: ${settingsSate.interfaceType}'),
          ],
        );
      },
    );
  }

  Widget testBlocConsumer() {
    return BlocConsumer<DataCubit, DataState>(
      listener: (context, dataState) {
        // do stuff here based on state
      },
      builder: (context, dataState) {
        // return widget here based on state
        return Text('BlocConsumer / $dataState');
      },
    );
  }

  Widget testBlocListener() {
    return BlocListener<DataCubit, DataState>(
      listener: (context, dataState) {
        // do stuff here based on state
      },
    );
  }

  Widget testBlocBuilder() {
    return BlocBuilder<DataCubit, DataState>(
      builder: (context, dataState) {
        // return widget here based on state
        return Text('BlocBuilder / $dataState');
      },
    );
  }
}

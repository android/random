import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/cubit/activity/activity_cubit.dart';
import 'package:random/models/activity/activity.dart';
import 'package:random/ui/widgets/game/game_board.dart';

class PageGame extends StatefulWidget {
  const PageGame({super.key});

  @override
  State<PageGame> createState() => _PageGameState();
}

class _PageGameState extends State<PageGame> {
  Widget buildGameActionsBloc(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(builder: (context, activityState) {
      final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

      final List<Widget> buttons = [
        IconButton(
          onPressed: () {
            activityCubit.updateState(Activity.createNew());
          },
          icon: const Icon(UniconsSolid.star),
          color: Theme.of(context).colorScheme.primary,
        )
      ];

      if (activityState.currentActivity.isRunning == true) {
        buttons.add(IconButton(
          onPressed: () {
            final Activity currentActivity = activityCubit.state.currentActivity;
            currentActivity.stop();

            activityCubit.updateState(currentActivity);
            setState(() {});
          },
          icon: const Icon(UniconsLine.exit),
          color: Theme.of(context).colorScheme.primary,
        ));
      }

      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: buttons,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    const double boardWidgetWidth = 300;
    const double boardWidgetHeight = 300;

    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (context, activityState) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            activityState.currentActivity.isRunning == true
                ? GameBoardWidget(
                    activity: activityState.currentActivity,
                    widgetSize: const Size(boardWidgetWidth, boardWidgetHeight),
                  )
                : SizedBox.shrink(),
            buildGameActionsBloc(context),
          ],
        );
      },
    );
  }
}

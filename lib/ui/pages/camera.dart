import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/ui/widgets/take_picture_widget.dart';

class PageCamera extends StatelessWidget {
  const PageCamera({super.key});

  static Icon navBarIcon = const Icon(UniconsLine.camera);
  static String navBarText = 'page_camera';

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).colorScheme.surface,
      child: const Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(height: 8),
          TakePictureWidget(),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/cubit/activity/api_cubit.dart';
import 'package:random/models/api_status.dart';
import 'package:random/ui/widgets/custom_title.dart';
import 'package:random/ui/widgets/api_data.dart';

class PageApi extends StatelessWidget {
  const PageApi({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Theme.of(context).colorScheme.surface,
      child: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 4),
        physics: const BouncingScrollPhysics(),
        children: <Widget>[
          const SizedBox(height: 8),
          const CustomTitle(text: 'api_page_title'),
          const SizedBox(height: 20),
          BlocBuilder<ApiDataCubit, ApiDataState>(
            builder: (BuildContext context, ApiDataState apiDataState) {
              return ApiDataWidget(
                data: apiDataState.data,
                status: (apiDataState is ApiDataFetchLoading)
                    ? ApiStatus.loading
                    : (apiDataState is ApiDataFetchLoaded)
                        ? ApiStatus.loaded
                        : ApiStatus.failed,
              );
            },
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/ui/painters/graph_painter.dart';

class PageGraph extends StatefulWidget {
  const PageGraph({super.key});

  @override
  State<PageGraph> createState() => _PageGraphState();
}

class _PageGraphState extends State<PageGraph> {
  double _currentSliderValue = 20;

  @override
  Widget build(BuildContext context) {
    double boardWidth = MediaQuery.of(context).size.width;

    return SizedBox.expand(
      child: FittedBox(
        fit: BoxFit.contain,
        alignment: Alignment.center,
        child: SizedBox(
          height: (MediaQuery.of(context).size.height),
          width: (MediaQuery.of(context).size.width),
          child: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: GestureDetector(
                    onTapUp: (details) {
                      double xTap = details.localPosition.dx;
                      double yTap = details.localPosition.dy;
                      printlog('[$xTap,$yTap]');
                    },
                    child: Container(
                      margin: const EdgeInsets.all(4),
                      padding: const EdgeInsets.all(4),
                      child: CustomPaint(
                        size: Size(boardWidth, boardWidth),
                        willChange: false,
                        painter: GraphPainter(linesCount: _currentSliderValue.toInt()),
                      ),
                    ),
                  ),
                ),
                Slider(
                  value: _currentSliderValue,
                  min: 10,
                  max: 50,
                  divisions: 5,
                  label: _currentSliderValue.round().toString(),
                  onChanged: (double value) {
                    setState(() {
                      _currentSliderValue = value;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

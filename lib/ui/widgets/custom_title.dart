import 'package:flutter_custom_toolbox/flutter_toolbox.dart';
import 'package:flutter/material.dart';

import 'package:random/cubit/activity/data_cubit.dart';
import 'package:random/cubit/activity/api_cubit.dart';
import 'package:random/cubit/settings/settings_cubit.dart';
import 'package:random/models/interface_type.dart';

class CustomTitle extends StatelessWidget {
  const CustomTitle({super.key, required this.text});

  final String text;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          tr(text),
          textAlign: TextAlign.start,
          style: Theme.of(context).textTheme.headlineMedium!.apply(fontWeightDelta: 2),
        ),
        const SizedBox(width: 2),
        expertInterfaceIndicator(),
        const SizedBox(width: 2),
        dataCounterIndicator(),
        const SizedBox(width: 2),
        apiLoadingIndicator(),
      ],
    );
  }

  Widget expertInterfaceIndicator() {
    return BlocBuilder<SettingsCubit, SettingsState>(
      builder: (BuildContext context, SettingsState settingsState) {
        bool isExpert = settingsState.interfaceType == InterfaceType.expert;

        return Text(isExpert ? '⭐' : '');
      },
    );
  }

  Widget dataCounterIndicator() {
    return BlocBuilder<DataCubit, DataState>(
      builder: (context, dataState) {
        return Text('(${dataState.counter})');
      },
    );
  }

  Widget apiLoadingIndicator() {
    return BlocBuilder<ApiDataCubit, ApiDataState>(
      builder: (context, apiDataState) {
        return (apiDataState is ApiDataFetchLoading)
            ? const Text('⏳')
            : (apiDataState is ApiDataFetchLoaded)
                ? const Text('✅')
                : const Text('⚠️');
      },
    );
  }
}

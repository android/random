import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/widgets/show_error.dart';

import 'package:random/models/api_data.dart';
import 'package:random/models/api_status.dart';

class ApiDataWidget extends StatelessWidget {
  const ApiDataWidget({
    super.key,
    required this.status,
    required this.data,
  });

  final ApiStatus status;
  final ApiData? data;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('status: $status'),
        Text('number: ${data?.number.toString() ?? ''}'),
        Text('md5: ${data?.md5.toString() ?? ''}'),
        Text('updatedAt: ${data?.updatedAt.toString() ?? ''}'),
        status == ApiStatus.loading
            ? const SizedBox(
                width: 12,
                height: 12,
                child: CircularProgressIndicator(),
              )
            : const SizedBox.shrink(),
        status == ApiStatus.failed
            ? const ShowErrorWidget(message: 'state.failure.message')
            : const SizedBox.shrink(),
      ],
    );
  }
}

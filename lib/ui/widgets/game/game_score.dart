import 'package:flutter/material.dart';

import 'package:random/models/activity/activity.dart';

class GameScoreWidget extends StatelessWidget {
  const GameScoreWidget({
    super.key,
    required this.activity,
  });

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: const EdgeInsets.all(5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text('Settings:'),
          Text('  ${activity.activitySettings}'),
          const Text('Game:'),
          Text('  isRunning: ${activity.isRunning}'),
          Text('  isFinished: ${activity.isFinished}'),
          Text('  movesCount: ${activity.movesCount}'),
          Text('  score: ${activity.score}'),
        ],
      ),
    );
  }
}

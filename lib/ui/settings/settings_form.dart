import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/cubit/settings/settings_cubit.dart';
import 'package:random/models/interface_type.dart';

class SettingsForm extends StatefulWidget {
  const SettingsForm({super.key});

  @override
  State<SettingsForm> createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {
  final apiUrlController = TextEditingController();
  final securityTokenController = TextEditingController();
  final List<Widget> interfaceTypesWidgets = InterfaceTypes.selectWidgets;

  @override
  void dispose() {
    apiUrlController.dispose();
    securityTokenController.dispose();
    super.dispose();
  }

  List<bool> _selectedInterfaceType = <bool>[];

  @override
  Widget build(BuildContext context) {
    SettingsCubit settings = BlocProvider.of<SettingsCubit>(context);

    apiUrlController.text = settings.getApiUrl();
    securityTokenController.text = settings.getSecurityToken();
    if (_selectedInterfaceType.length != 2) {
      _selectedInterfaceType = <bool>[
        settings.getInterfaceType() == InterfaceType.basic,
        settings.getInterfaceType() == InterfaceType.expert,
      ];
    }

    void saveSettings() {
      BlocProvider.of<SettingsCubit>(context).setValues(
        apiUrl: apiUrlController.text,
        securityToken: securityTokenController.text,
        interfaceType: _selectedInterfaceType[0] ? InterfaceType.basic : InterfaceType.expert,
      );
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        // Light/dark theme
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            const Text('settings_label_theme').tr(),
            const Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ApplicationSettingsThemeModeCard(
                  mode: ThemeMode.system,
                  icon: UniconsLine.cog,
                ),
                ApplicationSettingsThemeModeCard(
                  mode: ThemeMode.light,
                  icon: UniconsLine.sun,
                ),
                ApplicationSettingsThemeModeCard(
                  mode: ThemeMode.dark,
                  icon: UniconsLine.moon,
                )
              ],
            ),
          ],
        ),

        const SizedBox(height: 16),

        const Text('settings_label_api_url').tr(),
        TextFormField(
          controller: apiUrlController,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
          ),
        ),

        const SizedBox(height: 16),

        const Text('settings_label_security_token').tr(),
        TextFormField(
          controller: securityTokenController,
          decoration: const InputDecoration(
            border: UnderlineInputBorder(),
          ),
        ),

        const SizedBox(height: 16),

        const Text('settings_label_interface_type').tr(),
        ToggleButtons(
          direction: Axis.horizontal,
          onPressed: (int index) {
            setState(() {
              _selectedInterfaceType = index == 0 ? [true, false] : [false, true];
            });
          },
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          selectedBorderColor: appTheme.primaryColor,
          selectedColor: appTheme.colorScheme.onSecondary,
          fillColor: appTheme.colorScheme.secondary,
          color: appTheme.primaryColor,
          constraints: const BoxConstraints(
            minHeight: 40.0,
            minWidth: 80.0,
          ),
          isSelected: _selectedInterfaceType,
          children: interfaceTypesWidgets,
        ),
        const SizedBox(height: 20),
        ElevatedButton(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Icon(UniconsLine.save),
              const SizedBox(width: 8),
              const Text('settings_button_save').tr(),
            ],
          ),
          onPressed: () => saveSettings(),
        ),
      ],
    );
  }
}

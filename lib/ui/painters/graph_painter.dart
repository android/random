import 'dart:math';

import 'package:flutter/material.dart';

import 'package:random/config/app_colors.dart';

class GraphPainter extends CustomPainter {
  const GraphPainter({required this.linesCount});

  final int linesCount;

  double random(double max) {
    return 0.05 * max + Random().nextDouble() * max * 0.9;
  }

  Color getRandomColor() {
    const List<Color> availableColors = [
      AppColors.contentColorCyan,
      AppColors.contentColorGreen,
      AppColors.contentColorOrange,
      AppColors.contentColorPurple,
      AppColors.contentColorYellow,
      AppColors.contentColorPink,
      AppColors.contentColorWhite,
      AppColors.mainTextColor3,
    ];

    return availableColors[Random().nextInt(availableColors.length)];
  }

  @override
  void paint(Canvas canvas, Size size) {
    // Fill background
    final paintBackground = Paint();
    paintBackground.color = Colors.black;
    paintBackground.style = PaintingStyle.fill;

    final Rect rectBackground =
        Rect.fromPoints(const Offset(0, 0), Offset(size.width, size.height));
    canvas.drawRect(rectBackground, paintBackground);

    // Draw some lines
    Paint paintLine = Paint();
    paintLine.style = PaintingStyle.fill;

    for (int i = 0; i < linesCount; i++) {
      paintLine.color = getRandomColor();
      paintLine.strokeWidth = Random().nextDouble() * 4 + 2;

      Offset lineStart = Offset(random(size.width), random(size.height));
      Offset lineStop = Offset(random(size.width), random(size.height));

      canvas.drawLine(lineStart, lineStop, paintLine);
      canvas.drawCircle(lineStart, 8, paintLine);
      canvas.drawCircle(lineStop, 8, paintLine);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/config/app_colors.dart';

class CellPainter extends CustomPainter {
  const CellPainter({required this.value});

  final int value;

  Color getIndexedColor(int index) {
    const List<Color> availableColors = [
      AppColors.contentColorCyan,
      AppColors.contentColorGreen,
      AppColors.contentColorOrange,
      AppColors.contentColorPurple,
      AppColors.contentColorYellow,
      AppColors.contentColorPink,
      AppColors.contentColorWhite,
    ];

    return availableColors[index % availableColors.length];
  }

  @override
  void paint(Canvas canvas, Size size) {
    final baseColor = getIndexedColor(value);

    const borderWidth = 0.05;

    final Rect baseSquare =
        Rect.fromPoints(const Offset(0, 0), Offset(size.width, size.height));

    final paintBaseSquare = Paint()
      ..style = PaintingStyle.fill
      ..color = baseColor.darken(40);

    canvas.drawRect(baseSquare, paintBaseSquare);

    final Rect innerGradientBackground = Rect.fromPoints(
        Offset(size.width * borderWidth, size.height * borderWidth),
        Offset(size.width * (1 - borderWidth), size.height * (1 - borderWidth)));

    final paintInnerBackground = Paint()
      ..shader = ui.Gradient.linear(
        baseSquare.topCenter,
        baseSquare.bottomCenter,
        [
          baseColor.lighten(10),
          baseColor.darken(20),
        ],
      );

    canvas.drawRect(innerGradientBackground, paintInnerBackground);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:random/models/api_data.dart';
import 'package:random/network/api.dart';

class ApiRepository {
  const ApiRepository({required this.apiService});

  final ApiService apiService;

  Future<ApiData> getApiData() async {
    printlog('(getApiData) delayed API call...');
    final response = await Future.delayed(const Duration(milliseconds: 1000))
        .then((value) => apiService.getData());
    if (response != null) {
      printlog('(getApiData) got api response');
      printlog(response.data);
      return ApiData.fromJson(response.data);
    }
    printlog('(getApiData) failed');
    return ApiData.fromJson({});
  }
}
